%clear all
%clc
%close all
addpath('mprgp');
addpath('utils');

load("data_8_4_reg.mat")

% .5 u'Ku - u'b s.t. B_e u=c_e and B_i u <= c_i

% .5 lambda'*B'*Kplus*B - lambda'*d s.t. G*lambda = v and lambda_I >=0
% B = [B_e; B_i]; 
F=Bt'*Kplus*Bt;
G=R'*Bt;

% homogenize EQ
sv = G'*((G*G')\v);
d = d - F*sv;
lb = lb - sv;
v = zeros(length(v),1);

% project on coarse space
Q=G'*((G*G')\G);
P=speye(size(Q))-Q;
H = P*F*P;
d = P*d;

%smalxe
params.rho = 1.1;
params.rho_update = 1.;
params.M = 10;
params.M_update = 10.;
params.eta = 1e-1;
params.rtol = 1e-4;
params.maxit = 100;
params.critflg = 0; % stopping criterion
params.print = 1; % 0 for print
params.implicit_orth = true;

%mprgp
mprgpctx.lb = lb;
mprgpctx.abarmult = 2.0;
mprgpctx.propConst = 1.0;
mprgpctx.settol = 10*eps;
mprgpctx.infeastol = 0;
mprgpctx.maxit = 1000;
%ipm
mprgpctx.method='direct';


%mprgpctx.maxeig=6.928472126187e+01;
%H1 = H+1e-14.*speye(size(lb,1));
[x,~,k] = smalxe(H,d,G,v,zeros(size(F,1),1),params,@mprgp,mprgpctx);k
%[x,~,k] = smalxe(H,d,G,v,x,params,@ipm_contact_freevar,mprgpctx);k
[x2,~,k] = smalxe(H,d,G,v,ones(size(F,1),1),params,@ipm_contact_freevar_cp,mprgpctx);k

norm(x-x2)
norm(x-x2)/norm(x)

%lb(lb<=-1e20)=-1e20;
%x3 = quadprog(H,-d,[],[],G,v,lb,[]);


