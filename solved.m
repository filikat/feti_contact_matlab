function [flg,convinfo] = solved(nrmg,x,ctx)
  nrmgeq = norm(ctx.B*x-ctx.c);
  switch ctx.critflg
    case 0
      innertol = min(ctx.M*nrmgeq,ctx.eta);
    case 1
      innertol = ctx.M*ctx.nrmb0;
    case 2
      innertol = ctx.M*ctx.nrmb;
    case 3
      innertol = max(min(ctx.M*nrmgeq,ctx.eta),ctx.outertol);
    case 4
      innertol = max(ctx.M*ctx.nrmb0,ctx.outertol);
    case 5
      innertol = max(ctx.M*ctx.nrmb,ctx.outertol);
    case 9
      innertol = ctx.outertol;
  end
  flg = 0; % did not converge
  if nrmg <= ctx.outertol && nrmgeq <= ctx.outertol
    flg = 2; % happy breakdown
  elseif nrmg <= innertol
    flg = 1; % innersolver convergence
  end
  convinfo = [nrmg,nrmgeq,innertol];
  if ctx.print
    fprintf('%e %e %e %e\n',nrmg,nrmgeq,innertol,ctx.outertol);
  end
end

