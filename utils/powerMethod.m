function [ lambda, k] = powerMethod( A,x,tol,maxit )
%powerMethod - Estimate the largest eigenvalue
% https://en.wikipedia.org/wiki/Power_iteration
  lambda = norm(x);
  x = x/lambda;
  for k = 1:maxit
    lambda0 = lambda;
    x = matvec(A,x);
    lambda = norm(x);
    if abs(lambda-lambda0) < tol
      break;
    end
    x = x/lambda;
  end
end
