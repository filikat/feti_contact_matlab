%title of figures
% plot_A_TFETI_lin_P_1
% hist_F_TFETI_nonlin_Pless_10

nnzeps = 1e-14;

% plot settings
n_interval = 20;
max_y_axis = 10^4;
max_coup = 4e-8; min_coup = 0;
length_interval = max_coup - min_coup;
sub = length_interval/n_interval;
spec_vec = min_coup:sub:max_coup;

addpath('~/devel/petsc/share/petsc/matlab');
Kplus = PetscBinaryRead('~/devel/permoncube/Kplus.bin');
Bt = PetscBinaryRead('~/devel/permoncube/Bt.bin');
R = PetscBinaryRead('~/devel/permoncube/R.bin');

%Kplus = PetscBinaryRead("~/devel/permon/examples/feti/Kplus.bin");
%Bt = PetscBinaryRead("~/devel/permon/examples/feti/Bt.bin");
%R = PetscBinaryRead("~/devel/permon/examples/feti/R.bin");

%Kplus = PetscBinaryRead("~/devel/permonbac/permonmembrane/Kplus.bin");
%Bt = PetscBinaryRead("~/devel/permonbac/permonmembrane/Bt.bin");
%R = PetscBinaryRead("~/devel/permonbac/permonmembrane/R.bin");

%Kplus(50:100,50:100)

%for contact benchmark
G=R'*Bt;
Q=G'*((G*G')\G);
P=speye(size(Q))-Q;
F=Bt'*Kplus*Bt;
rho=10*eigs(F,1)   % penaltu dej jako deseti nasobek nejvet. vlastniho cisla F

%Aeigs = eig(full((speye(size(Kplus))-R*R')*Kplus));
%fprintf('Kplus: min %.4e max %.4e\n',Aeigs(1),Aeigs(end));

Aeigs = sort(real(eig(full(F))));
fprintf('F: min %.4e minnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'0_hist_F','png'); close;
size(Aeigs(Aeigs == 1e-14),1)

A = P*F*P;
Aeigs = sort(real(eig(full(A))));
%Aeigs(Aeigs<=eps) = 0;
fprintf('PFP: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'1_hist_PFP','png'); close;
size(Aeigs(Aeigs == 1e-14),1)

Aeigs = sort(real(eig(full(G'*G))));
fprintf('GtG: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'2_hist_GtG','png'); close;

Aeigs = sort(real(eig(full(Q))));
fprintf('Q: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'3_hist_Q','png'); close;

A = F + rho*(G'*G);
Aeigs = sort(real(eig(full(A))));
%Aeigs(Aeigs<1e-14) = 0;
fprintf('F10rGtG: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'4_hist_F10rGtG','png'); close;

A = F + rho*Q;
Aeigs = sort(real(eig(full(A))));
%Aeigs(Aeigs<=eps) = 0;
fprintf('F10rQ: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'5_hist_F10rQ','png'); close;
size(Aeigs(Aeigs == 1e-14),1)

A = P*F*P + rho*(G'*G);
Aeigs = sort(real(eig(full(A))));
%Aeigs(Aeigs<1e-14) = 0;
fprintf('PFP10rGtG: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'6_hist_PFP10rGtG','png'); close;

A = P*F*P + rho*Q;
Aeigs = sort(real(eig(full(A))));
%Aeigs(Aeigs<=eps) = 0;
fprintf('PFP10rQ: min %.4e minnnz %.4e max %.4e\n',Aeigs(1),Aeigs(find(Aeigs>nnzeps,1)),Aeigs(end));
Aeigs(Aeigs<=nnzeps) = 1e-14;
fig = figure;plot(Aeigs,'bo');
set(gca, 'YScale', 'log','fontsize',20);
%fig = figure;histogram(abs(Aeigs),spec_vec);
%set(gca, 'YScale', 'log','fontsize',20);
%ylim([10^0 max_y_axis]);
saveas(fig,'7_hist_PFP10rQ','png'); close;
size(Aeigs(Aeigs == 1e-14),1)

imagesc(abs(full(P*F*P)))
