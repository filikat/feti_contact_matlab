function [x,flg,k,mu,convinfo] = smalxe(A,b,B,c,x0,params,innersolver,varargin)
% smalxe(A,b,B,c,x0,10,1,10,10,1e-1,1e-6,innersolver,...)
% innersolver = @cgCommon
% ... innersolver arguments

%eta=norm(b0);
rho = params.rho;
rho_update = params.rho_update;
M = params.M;
M_update = params.M_update;
eta = params.eta;
rtol = params.rtol;
maxit = params.maxit;

x = x0;
b0 = b;
maxeig = eigs(A,1);
%maxeig = 6.928472126187e+00;
rho = rho*maxeig;
M = M*maxeig;
eta = eta*norm(b0);
if params.implicit_orth 
  Q=B'*((B*B')\B);
else
  Q = B'*B;
end
Apenalized = A + rho*Q;
mu = zeros(size(B,1),1); 
%mu=mu+rho*(B*x-c);

% convergence ctx
ctx.outertol = rtol*norm(b0);
ctx.nrmb0 = norm(b0);
ctx.B = B;
ctx.c = c;
ctx.M = M;
ctx.eta = eta;
ctx.rhochanged = 1;
ctx.critflg = params.critflg;
ctx.print = params.print;

kinner = 0;
kouter = 0;
b = b0 - applyHalfQ(B,mu + rho*c,params.implicit_orth); 
ctx.nrmb = norm(b);
lag = 0.5*x'*Apenalized*x - b'*x;

ctx.dA = diag(Apenalized);

while kouter < maxit
  fprintf('%e %e %e\n',M,rho,eta)
  % minimize
  [x,flg,~,k,cinfo] = innersolver(Apenalized,b,x,ctx,varargin{:});
  if ~kinner
    convinfo = cinfo;
    kinner = k;
  else
    convinfo = [convinfo;cinfo];
    kinner = kinner + k;
  end
  kouter = kouter + 1;
  k = [kouter, kinner];

  % outer stopping criteria
  if ~flg 
    fprintf('Inner solver diverged\n');
    return
  elseif flg == 2
    flg = 1;
    return
  end

  %% compute Lagrangian
  lag_old = lag;
  lag = 0.5*x'*Apenalized*x -b'*x;
  increment = 0.5*rho*norm(B*x - c)^2;
  
  % update multiplicator
  mu = mu + rho*(B*x - c);
  %mu = mu + (B*x - c);

  % update M, rho
  if lag - lag_old < increment   
    M = M/M_update;
    ctx.M = M;
    if rho_update ~= 1.
      rho = rho*rho_update; 
      Apenalized = A + rho*Q;
      ctx.rhochanged = 1;
    end
  end

  % update inner rhs
  b = b0 - applyHalfQ(B,mu + rho*c,params.implicit_orth); 
  ctx.nrmb = norm(b);
end

flg = 0;
end

function y = applyHalfQ(B,x,implicit)
  if implicit
    y = B'*((B*B')\x);
  else
    y = B'*x;
  end
end


