function [x,flg,iter,iterPCG,cinfo]=ipm_contact_freevar(A,b,x,convCtx,ipmCtx)
% % % % % % % % % % % % % % % % % % % % % % % % % % %
% IPM method for contact problems.
% Solves:
% 
% min .5x'*A*x-b'*x
% s.t. x>=l
%
% Input
% A:        can be a function handle or a matrix
% b:        n vector
% l:        n vector
% method:   'pcg' or 'direct'
% dA:       diagonal of A (only for 'pcg')
%
% % % % % % % % % % % % % % % % % % % % % % % % % % %

method = ipmCtx.method;
dA = convCtx.dA;
l = ipmCtx.lb;

%% Free/constrained variables
n=length(b);
free_var=(l<-1e20);
con_var=logical(ones(n,1)-free_var);
n_con_var=sum(con_var);

%% Initialize
%initialization
x=ones(n,1);
%s=ones(n,1);
s=zeros(n,1);
s(con_var)=1;
iter = 0;
alpha_x = 0;
alpha_s = 0;
IPM_time=0;
iterPCG=0;

%parameters
tol=1e-6;
maxit=100;
sigma1   = 0.5;
sigmamin = 0.05;
sigmamax = 0.95;
rho = 0.995;
cgtol=1e-6;
cgmaxit=10;

%% Iteration
% % % % % % % % % % % % % % % % % % % % % % % % % %
% Newton system
%   [ A   -I ] (dx) = r1 = s+b-A*x
%   [ S  X-L ] (ds) = r2 = mu*e-S*(X-L)*e
%
% mu=(x-l)'*s/n;
% % % % % % % % % % % % % % % % % % % % % % % % % %  

r1 = s-applyA(A,x)+b;
mu = (x(con_var)-l(con_var))'*s(con_var)/n_con_var;

fprintf('\n ===== START IPM ===================================== \n')


fprintf('\nIPM_iter    infeas       mu     PCG_iter   PCG_flag\n')

while iter<maxit
    
    %[flg,cinfo] = solved(norm(r1),x,convCtx);
    %[flg,cinfo] = solved(max(norm(r1),mu*norm(b)),x,convCtx);
    %MPGP crit
    %g = applyA(A,x) - b;
    %[gf,gc] = mprgpSplit(x,g,ipmCtx);
    %[flg,cinfo] = solved(norm(gf+gc),x,convCtx);
    %if (flg)
    %  break
    %end
    if norm(r1)/norm(b)<tol && mu<tol
       flg = 2;
       fprintf('\n*** Optimal solution found ***\n')
       break
    end
    
    iter=iter+1;
    if iter == 1
      sigma = sigma1;
    else
      sigma = max(1-alpha_x,1-alpha_s)^5;
    end
    sigma = min(sigma,sigmamax);
    sigma = max(sigma,sigmamin);
    r2 = sigma*mu*ones(n_con_var,1) - (x(con_var)-l(con_var)).*s(con_var);
    
    %% Solve normal equations
    % % % % % % % % % % % % % % % % % % % % % % % % % %
    % Normal equations
    % (A+(X-L)^-1*S)*dx = r1+(X-L)^-1*r2
    % ds = (X-L)^-1*(r2-S*dx)
    % % % % % % % % % % % % % % % % % % % % % % % % % %
    
    tic
    rhs=r1;
    rhs(con_var)=rhs(con_var)+r2./(x(con_var)-l(con_var));
    Thetainv=zeros(n,1);
    Thetainv(con_var)=s(con_var)./(x(con_var)-l(con_var));
    Thetainv(free_var)=0;
    switch method
        case 'direct'
            if isa(A,'function_handle')
                error('Explicit A is needed for direct solution')
            end
            %AP = pinv(A+diag(Thetainv),1e-12);
            dx=(A+diag(Thetainv))\rhs;
            %dx=AP*rhs;
            iterpcg=0;flag=0;
        case 'pcg'
            [dx,flag,~,iterpcg]=pcg(@(y)applyA(A,y)+Thetainv.*y,rhs,cgtol,cgmaxit,@(y)y./(dA+Thetainv));
    end
    ds=zeros(n,1);
    ds(con_var)=(r2-dx(con_var).*s(con_var))./(x(con_var)-l(con_var));
    itertime=toc;
    
    IPM_time=IPM_time+itertime;
    iterPCG=iterPCG+iterpcg;
    
    %% Find steplength
    idx=false(n,1);
    ids=false(n,1);
    idx(con_var) = dx(con_var) < 0;
    ids(con_var) = ds(con_var) < 0;
    alphamax_x = min([1;(l(idx)-x(idx))./dx(idx)]);
    alphamax_s = min([1;-s(ids)./ds(ids)]);
    alpha_x = rho*alphamax_x;
    alpha_s = rho*alphamax_s;
    
    %step
    x = x+alpha_x*dx; 
    s = s+alpha_s*ds;
    
    if alpha_x<.1
    %keyboard
    end
    
    %% Prepare next iteration
    r1=s-applyA(A,x)+b;
    mu=(x(con_var)-l(con_var))'*s(con_var)/n_con_var;

    %print output
    fprintf('%6d   %10.2e %10.2e %7.2f %7.2f\n',iter,norm(r1)/norm(b),mu,alpha_x,alpha_s)
    
end

%% Print
fprintf('\nTime: %f\n',IPM_time)
fprintf('IPM iter: %d\n',iter)
fprintf('PCG iter: %d\n',iterPCG)

fprintf('\n ===== END IPM ===================================== \n')

end

function y=applyA(A,x)
    if isa(A,'function_handle')
        y=A(x);
    else
        y=A*x;
    end
end

