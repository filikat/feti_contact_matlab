function y=matvec(A,x)
    if isa(A,'function_handle')
        y=A(x);
    else
        y=A*x;
    end
end
