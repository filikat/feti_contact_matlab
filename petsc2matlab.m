addpath('~/devel/petsc/share/petsc/matlab');
p.Kplus = PetscBinaryRead('~/devel/permoncube/Kplus.bin');
p.Bt = PetscBinaryRead('~/devel/permoncube/Bt.bin');
p.R = PetscBinaryRead('~/devel/permoncube/R.bin');
p.d = PetscBinaryRead('~/devel/permoncube/d.bin');
p.v = PetscBinaryRead('~/devel/permoncube/e.bin');
p.lb = PetscBinaryRead('~/devel/permoncube/lb.bin');

save("data.mat","-V7","-struct","p");
